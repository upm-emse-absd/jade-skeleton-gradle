package es.upm.emse.absd.agents;

import jade.core.Agent;

public class HelloAgent extends Agent {

    protected void setup()
    {
        System.out.println("Hello!!! My name is " + this.getLocalName() + ". Nice to meet you :D");
    }

}
